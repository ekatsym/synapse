(in-package :asdf-user)

(defsystem synapse
  :author "ekatsym <ekatsymeee@gmail.com>"
  :maintainer "ekatsym <ekatsymeee@gmail.com>"
  :license "MIT"
  :version "0.0.1"
  :depends-on (:cl-cuda
               :mgl-mat
               :alexandria
               :optima)
  :components ((:module "src"
                :serial t
                :components
                ((:file "util")
                 (:file "option")
                 (:module "layer"
                  :serial t
                  :components
                  ((:file "package")
                   (:file "affine")
                   (:file "activation")
                   (:file "output")))
                 (:module "nn"
                  :serial t
                  :components
                  ((:file "fnn")
                   (:file "cnn")
                   (:file "rnn")))
                 (:file "dataset")
                 (:file "package")))))
