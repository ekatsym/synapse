(defpackage synapse.option
  (:use :cl
        :cl-cuda
        :mgl-mat
        :synapse.util)
  (:import-from :optima
                #:match)
  (:export #:*default-mat-ctype*
           #:*cuda-enabled*
           )
  )

(in-package :synapse.option)

(setf *default-mat-ctype* :float)
(setf *cuda-enabled* t)
