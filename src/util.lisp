(defpackage synapse.util
  (:use :cl
        :cl-cuda
        :mgl-mat)
  (:import-from :optima
                #:match)
  (:import-from :alexandria
                #:lastcar
                )
  (:export #:transpose-list
           #:iota
           #:build-list
           #:random-nth
           #:remove-nth
           #:list-shuffle
           #:compose-list
           #:compose-map
           #:make-random-mat
           #:lastcar
           )
  )

(in-package :synapse.util)

;;; list
(defun transpose-list (lists)
  (declare (optimize (speed 3)))
  (labels ((rec (lsts acc)
             (if (or (some #'null lsts) (null lsts))
                 acc
                 (rec (mapcar #'cdr lsts) (cons (mapcar #'car lsts) acc)))))
    (nreverse (rec lists nil))))

(defun iota (size &optional (start 0) (step 1))
  (labels ((rec (i v acc)
             (if (zerop i)
                 acc
                 (rec (1- i) (+ v step) (cons v acc)))))
    (rec size start nil)))

(defun build-list (size &optional (function #'values))
  (labels ((rec (i acc)
             (if (zerop i)
                 acc
                 (rec (1- i) (cons (funcall function (1- i))
                                   acc)))))
    (rec size nil)))

(defun random-nth (list)
  (nth (random (length list)) list))

(defun remove-nth (n list)
  (labels ((rec (n lst acc)
             (if (zerop n)
                 (nconc (nreverse acc) (cdr lst))
                 (rec (1- n) (cdr lst) (cons (car lst) acc)))))
    (rec n list nil)))

(defun list-shuffle (list)
  (labels ((rec (lst acc)
             (if (null lst)
                 acc
                 (let ((n (random (length lst))))
                   (rec (remove-nth n lst) (cons (nth n lst) acc))))))
    (rec list nil)))

;;; function
(defun compose-list (functions)
  (labels ((rec (fns acc)
             (if (null fns)
                 acc
                 (rec (cdr fns) (lambda (x) (funcall (car fns) (funcall acc x)))))))
    (rec functions #'values)))

(defun compose-map (functions)
  (lambda (&rest args)
    (mapcar (lambda (fn) (apply fn args))
            functions)))

;;; mat
(defun make-random-mat (dimensions &key (mean 0) (stddev 1))
  (gaussian-random! (make-mat dimensions)
                    :mean mean
                    :stddev stddev))
