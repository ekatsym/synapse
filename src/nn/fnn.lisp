(defpackage synapse.fnn
  (:use :cl
        :cl-cuda
        :mgl-mat
        :synapse.util
        :synapse.option
        :synapse.layer
        )
  (:import-from :optima
                #:match)
  (:export #:fnn
           #:layers
           #:forward
           #:backward
           #:clear
           #:update
           #:initialize
           #:loss))

(in-package :synapse.fnn)

(defstruct fnn
  (layers     nil :type list)
  (forward    nil :type function)
  (backward   nil :type function)
  (clear      nil :type function)
  (update     nil :type function)
  (initialize nil :type function)
  (loss       nil :type function))

(defun fnn (&rest layers)
  (match (mapcar (lambda (lst) (remove nil lst))
                 (transpose-list
                   (mapcar (lambda (layer)
                             (match layer
                               ((affine forward backward clear update initialize)
                                (list forward backward clear update initialize nil))
                               ((or (relu forward backward clear)
                                    (sigmoid forward backward clear)
                                    (linear forward backward clear))
                                (list forward backward clear nil nil nil))
                               ((or (softmax-cross-entropy forward backward clear loss)
                                    (mean-square forward backward clear loss))
                                (list forward backward clear nil nil loss))))
                           layers)))
    ((list forwards backwards clears updates initializes losss)
     (make-fnn
       :layers      layers
       :forward     (compose-list forwards)
       :backward    (compose-list (nreverse backwards))
       :clear       (compose-map clears)
       :update      (compose-map updates)
       :initialize  (compose-map initializes)
       :loss        (lastcar losss)))))

(defun forward (fnn mat)
  (funcall (fnn-forward fnn) mat))
(defun backward (fnn mat)
  (funcall (fnn-backward fnn) mat))
(defun clear (fnn)
  (funcall (fnn-clear fnn)))
(defun update (fnn learning-rate batch-size)
  (funcall (fnn-update fnn) learning-rate batch-size))
(defun initialize (fnn &optional new-weight new-bias)
  (funcall (fnn-initialize fnn) new-weight new-bias))
(defun loss (fnn)
  (funcall (fnn-loss fnn)))
