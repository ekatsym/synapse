(defpackage synapse.dataset
  (:use :cl
        :cl-cuda
        :mgl-mat
        :synapse.util
        :synapse.option
        :synapse.layer
        :synapse.fnn)
  (:import-from :optima
                #:match)
  (:export #:dataset
           #:dsref
           #:learn
           #:print-fnn-result
           )
  )

(in-package :synapse.dataset)

(defstruct dataset
  (size 0 :type fixnum)
  (input-data (vector) :type simple-vector)
  (output-data (vector) :type simple-vector))

(defun dataset (input-data output-data)
  (make-dataset
    :size (length input-data)
    :input-data (map 'vector #'array-to-mat input-data)
    :output-data (map 'vector #'array-to-mat output-data)))

(defun dsref (dataset data index)
  (svref (slot-value dataset data) index))

(defun learn (fnn dataset count
                    &key (batch-size (dataset-size dataset)) (learning-rate 1.0e-6))
  (dotimes (counter count)
    (let ((batch-list (list-shuffle (iota batch-size))))
      (dolist (index batch-list)
        (let ((i-datum (dsref dataset 'input-data index))
              (o-datum (dsref dataset 'output-data index)))
          (forward fnn i-datum)
          (backward fnn o-datum)))
      (update fnn learning-rate batch-size))))

(defun print-fnn-result (dataset fnn)
  (dotimes (i (dataset-size dataset))
    (forward fnn (dsref dataset 'input-data i))
    (backward fnn (dsref dataset 'output-data i))
    (format t "~%INPUT:~8t~a~%OUTPUT:~8t~a~%TARGET:~8t~a~%LOSS:~8t~a~%"
            (mat-to-array (dsref dataset 'input-data i))
            (mat-to-array (match fnn
                            ((fnn layers)
                             (slot-value (lastcar layers) 'output))))
            (mat-to-array (dsref dataset 'output-data i))
            (loss fnn))))
