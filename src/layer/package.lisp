(defpackage synapse.layer
  (:use :cl
        :cl-cuda
        :mgl-mat
        :synapse.util
        :synapse.option)
  (:export #:affine
           #:activation
           #:relu
           #:sigmoid
           #:linear
           #:output
           #:softmax-cross-entropy
           #:mean-square
           #:forward
           #:backward
           #:clear
           #:update
           #:initialize
           #:loss

           #:affine-weight
           #:affine-bias
           ))
