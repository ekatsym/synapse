(in-package :synapse.layer)

(defstruct (affine (:print-object
                     (lambda (affine stream)
                       (format stream
                               "#<AFFINE ~a->~a>"
                               (affine-input-size affine)
                               (affine-output-size affine)))))
  (input-size     nil :type fixnum)
  (output-size    nil :type fixnum)
  (weight         nil :type mat)
  (bias           nil :type mat)
  (input          nil :type mat)
  (output         nil :type mat)
  (dloss/dinput   nil :type mat)
  (dloss/dweight  nil :type mat)
  (dloss/dbias    nil :type mat)
  (forward        nil :type function)
  (backward       nil :type function)
  (clear          nil :type function)
  (update         nil :type function)
  (initialize     nil :type function))

(defun affine (input-size output-size &optional (rand-mean 0) (rand-stddev 1))
  (let ((weight         (make-random-mat (list output-size input-size)
                                         :mean rand-mean :stddev rand-stddev))
        (bias           (make-random-mat (list output-size 1)
                                         :mean rand-mean :stddev rand-stddev))
        (input          (make-mat (list input-size 1)))
        (output         (make-mat (list output-size 1)))
        (dloss/dinput   (make-mat (list 1 input-size)))
        (dloss/dweight  (make-mat (list input-size output-size)))
        (dloss/dbias    (make-mat (list 1 output-size))))
    (labels ((forward (mat)
               (copy! mat input)
               (gemm! 1.0 weight input 0.0 output
                      :m output-size :n 1 :k input-size)
               (axpy! 1.0 bias output
                      :n output-size))
             (backward (mat)
               (copy! mat dloss/dbias)
               (gemm! 1.0 input mat 1.0 dloss/dweight
                      :m input-size :n output-size :k 1)
               (gemm! 1.0 mat weight 0.0 dloss/dinput
                      :m 1 :n input-size :k output-size))
             (clear ()
               (mapc (lambda (var) (fill! 0.0 var))
                     (list input output dloss/dinput dloss/dweight dloss/dbias)))
             (update (learning-rate batch-size)
               (axpy! (- (/ learning-rate batch-size))
                      (transpose dloss/dbias) bias
                      :n output-size)
               (axpy! (- (/ learning-rate batch-size))
                      (transpose dloss/dweight) weight
                      :n (* input-size output-size))
               (clear))
             (initialize (&optional new-weight new-bias)
               (let ((new-weight (or new-weight
                                     (make-random-mat (list output-size input-size)
                                                      :mean rand-mean :stddev rand-stddev)))
                     (new-bias (or new-bias
                                   (make-random-mat (list output-size)
                                                    :mean rand-mean :stddev rand-stddev))))
                 (copy! new-weight weight)
                 (copy! new-bias bias)
                 (clear))))
      (make-affine
        :input-size     input-size
        :output-size    output-size
        :weight         weight
        :bias           bias
        :input          input
        :output         output
        :dloss/dinput   dloss/dinput
        :dloss/dweight  dloss/dweight
        :dloss/dbias    dloss/dbias
        :forward        #'forward
        :backward       #'backward
        :clear          #'clear
        :update         #'update
        :initialize     #'initialize))))
