(in-package synapse.layer)

(deftype activation ()
  `(or relu sigmoid linear))

(defmacro define-activation (name &body label-definitions)
  `(prog1
     (defstruct ,name
       (size          nil :type fixnum)
       (input         nil :type mat)
       (output        nil :type mat)
       (dloss/dinput  nil :type mat)
       (forward       nil :type function)
       (backward      nil :type function)
       (clear         nil :type function))
     (defun ,name (size)
       (let ((input         (make-mat (list size 1)))
             (output        (make-mat (list size 1)))
             (dloss/dinput  (make-mat (list 1 size))))
         (labels ((clear ()
                    (mapc (lambda (var) (fill! 0.0 var :n size))
                          (list input output dloss/dinput)))
                  ,@label-definitions)
           (,(alexandria:symbolicate 'make- name)
             :size         size
             :input        input
             :output       output
             :dloss/dinput dloss/dinput
             :forward      #'forward
             :backward     #'backward
             :clear        #'clear))))))

(define-activation relu
  (forward (mat)
    (copy! mat input
           :n size)
    (copy! mat output
           :n size)
    (.max! 0.0 output))
  (backward (mat)
    (copy! mat dloss/dinput
           :n size)
    (dotimes (i size)
      (when (<= (mref output i 0) 0.0)
        (setf (mref dloss/dinput 0 i) 0)))
    dloss/dinput))

(define-activation sigmoid
  (forward (mat)
    (copy! mat input)
    (copy! mat output)
    (.logistic! output))
  (backward (mat)
    (copy! (transpose output) dloss/dinput
           :n size)
    (scal! -1.0 dloss/dinput
           :n size)
    (.+! 1.0 dloss/dinput)
    (geem! 1.0 dloss/dinput (transpose output) 0.0 dloss/dinput)
    (geem! 1.0 dloss/dinput mat 0.0 dloss/dinput)))

(define-activation linear
  (forward (mat) mat)
  (backward (mat) mat))
