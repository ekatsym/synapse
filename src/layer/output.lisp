(in-package :synapse.layer)

(deftype output ()
  `(or softmax-cross-entropy
       mean-square))

(defmacro define-output (name &body label-definitions)
  `(prog1
     (defstruct ,name
       (size          nil :type fixnum)
       (output        nil :type mat)
       (target        nil :type mat)
       (dloss/dinput  nil :type mat)
       (forward       nil :type function)
       (backward      nil :type function)
       (clear         nil :type function)
       (loss          nil :type function))
     (defun ,name (size)
       (let ((input         (make-mat (list size 1)))
             (output        (make-mat (list size 1)))
             (target        (make-mat (list size 1)))
             (dloss/dinput  (make-mat (list 1 size))))
         (labels ((clear ()
                    (mapc (lambda (var) (fill! 0.0 var :n size))
                          (list input output target dloss/dinput))) 
                  ,@label-definitions)
           (,(alexandria:symbolicate 'make- name)
             :size         size
             :output       output
             :target       target
             :dloss/dinput dloss/dinput
             :forward      #'forward
             :backward     #'backward
             :loss         #'loss
             :clear        #'clear))))))

(define-output softmax-cross-entropy
  (forward (mat)
    (copy! mat output
           :n size)
    (.exp! output
           :n size)
    (scal! (/ (asum output :n size))
           output))
  (backward (mat)
    (copy! mat target
           :n size)
    (m- output target))
  (loss ()
    (- (dot target
            (.log! (copy-mat output) :n size)
            :n size))))

(define-output mean-square
  (forward (mat) mat)
  (backward (mat)
    (copy! mat target
           :n size)
    (m- output target))
  (loss ()
    (/ (nrm2 (m- target output) :n size)
       2)))
