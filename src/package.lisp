(defpackage synapse
  (:use :cl
        :cl-cuda
        :mgl-mat
        :synapse.util
        :synapse.option
        :synapse.layer
        :synapse.dataset)
  (:export #:affine
           #:activation
           #:relu
           #:sigmoid
           #:linear
           #:output
           #:softmax-cross-entropy
           #:mean-square
           #:fnn
           #:layers
           )
  )
