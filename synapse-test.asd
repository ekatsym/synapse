(in-package :asdf-user)

(defsystem synapse-test
  :author "ekatsym <ekatsymeee@gmail.com>"
  :maintainer "ekatsym <ekatsymeee@gmail.com>"
  :license "MIT"
  :version "0.0.1"
  :depends-on (:cl-cuda
               :mgl-mat
               :alexandria
               :optima
               :synapse
               :fiveam)
  :perform (test-op (o s) (symbol-call :fiveam '#:run!
                                       (find-symbol* :affine
                                                     :affine-test
                                                     )))
  :components ((:module "t"
                :serial t
                :components
                ((:file "affine-test"))
                ))
  )
