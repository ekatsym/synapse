(defpackage affine-test
  (:use :cl
        :fiveam
        :mgl-mat
        :synapse.util
        :synapse.layer
        ))

(in-package :affine-test)

(def-suite affine)
(in-suite affine)

(test affine-test
  (let ((affine (affine 100 150))
        (mat (make-random-mat '(100 1))))
    (is (m= (funcall (slot-value affine 'forward) mat)
            (axpy! 1.0 (affine-bias affine)
                   (gemm! 1.0 (affine-weight affine) mat 0.0 (make-mat '(100 1))))))))

(run! 'affine-test)
